class bitwise{
	public static void main(String[] args){
		int x=6;
		int y=3;

		System.out.println(x&y);                 //0110 & 0011 =0010=2 
		System.out.println(x|y);                 //0111=7    
	        System.out.println(x^y);                 //0101=5
		System.out.println(x<<1);                //0000 0110= 0000 1100=12
		System.out.println(y>>1);                //0000 0011=0000 0001=1
	
	}
}
