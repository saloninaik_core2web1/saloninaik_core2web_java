
class bitwise{
        public static void main(String[] args){
                int x=10;                                //0000 1010=-11 we know to toggle means add 1 and give -ve sign
                System.out.println(~x);                  //1111 0101
                int y=0b1111111111111111111111110101;                           //our expected answer is given as input to y
                System.out.println(y);     
        }
}
//in this code we have taken 32 bits and now we will get same outpu

