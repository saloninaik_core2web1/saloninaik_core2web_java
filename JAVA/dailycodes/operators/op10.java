class logicalcode{
	public static void main(String[] args){
		int x=10;
		int y=12;
		System.out.println((x>y) &&(x<y));                   //this will not give error for bad operator as data type is in int bcz the answer to x>y is in boolean as it                                 //false  //true   //false                  is relational operator


                int a=10;
                int b=11;                                            		
		System.out.println((++a>=b) &&(a<++b));               //11>=11 &&  11<12
                                                                      // true       true    true

		int l=5;
	        int m=3;	                                          //l=4  //m=4    //m=3
	        System.out.println((l--<=++m) &&(++l<m--));         //5<=4     &&  5<4   
	     	                                                    //false   &&  false   false
	
	}
}
