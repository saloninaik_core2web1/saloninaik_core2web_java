class switchdemo{
        public static void main(String[] args){
                float num=7.5;
                System.out.println("before switch");
                switch(num){
                        case 1.5:
                                System.out.println("one");
                                break;
                        case 2.5:
                                System.out.println("two");
                                break;
                        case 3.5:
                                System.out.println("three");
                                break;
                        default:
                                System.out.println("in default state");
                         }
                System.out.println("after switch");
        }
}

