class switchdemo{
        public static void main(String[] args){
                char demo='B';
                System.out.println("before switch");
                switch(demo){
                        case 'A':
                                System.out.println("A");
                                break;
                        case 65:
                                System.out.println("65");
                                break;
                        case 'B':
                                System.out.println("B");
                                break;
			case 66:
				System.out.println("66");
                        default:
                                System.out.println("in default state");
                         }
                System.out.println("after switch");
        }
}

