class switchdemo{
        public static void main(String[] args){
                char demo='B';
                System.out.println("before switch");
                switch(demo){
                        case 'A':
                                System.out.println("A");
                                break;
                        case 'B':
                                System.out.println("B");
                                break;
                        case 'C':
                                System.out.println("C");
                                break;
                        default:
                                System.out.println("in default state");
                         }
                System.out.println("after switch");
        }
}

