
//in this program an error will occur stating lossy conversion from int to byte as the range of byte is -128 to 127;
//(thus we can say that bigger data cannot be stored in smaller data type)

class integerdemo{
	public static void main(String[] args){
		byte age=128;
		System.out.println(age);               //error
	}
}
