import java.util.*;

class pyramidPattern{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("how many rows :");
		int r=sc.nextInt();

		int num=1;
		for(int i=1;i<=r;i++){
			for(int space=r;space>i;space--){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				System.out.print(num+"\t");
			}
			System.out.println();
		}
	}
}



