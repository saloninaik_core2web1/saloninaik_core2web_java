import java.util.*;

class pattern{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.println("HOW MANY ROWS: ");
                int r=sc.nextInt();
	
		for(int i=1;i<=r;i++){
              		int num=1;
			int ch=97;
			for(int j=1;j<=r-i+1;j++){
				if(j%2==0){
					System.out.print((char)ch+" ");
					ch++;
				}
				else{
					System.out.print(num+" ");
					num++;
				}
			
			}
	
			System.out.println();
		}
	}
}
