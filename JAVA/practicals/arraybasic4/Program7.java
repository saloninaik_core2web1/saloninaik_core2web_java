import java.util.*;

class arraydemo{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("ENTER SIZE:");
                int size=sc.nextInt();
                char arr[]=new char[size];

                for(int i=0;i<size;i++){
                        arr[i]=sc.next().charAt(0);
                }

                for(int i=0;i<size;i++){
                        if((arr[i]>=(char)97 && arr[i]<=(char)120)){
				arr[i]=(char)(arr[i]-32);
				System.out.print((char)arr[i]+"  ");
                        }

                }
		System.out.println();
        }
}
