import java.util.*;

class twoDArray{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("HOW MANY ROWS: ");
                int r=sc.nextInt();
                System.out.print("HOW MANY COLUMNS: ");
                int c=sc.nextInt();
                int arr[][]=new int[r][c];
		int sum=0;
                System.out.println("ENTER ELEMENTS ");
                for(int i=0;i<r;i++){
                        for(int j=0;j<c;j++){
                                arr[i][j]=sc.nextInt();
                        }
                }

                for(int i=0;i<r;i++){
                        for(int j=0;j<c;j++){
                                System.out.print(arr[i][j] +"\t");
				sum=sum+arr[i][j];
                        }
                        System.out.println();
                }
		System.out.println("SUM OF ELEMENTS IS : "+sum);

        }
}

