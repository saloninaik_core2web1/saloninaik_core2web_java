import java.util.*;

class patterndemo{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("HOW MANY ROWS:");
                int r=sc.nextInt();
                int num=1;
                for(int i=1;i<=r;i++){

                        for(int space=1;space<i;space++){
                                System.out.print("\t");
                        }
                        for(int j=r;j>=i;j--){
                                System.out.print(num+"\t");
                                num++;
                        }
			num--;
                        System.out.println();
                }
        }
}

