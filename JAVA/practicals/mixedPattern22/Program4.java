import java.util.*;

class patterndemo{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("HOW MANY ROWS:");
                int r=sc.nextInt();
                int num=65;
                for(int i=1;i<=r;i++){

                        for(int space=1;space<i;space++){
                                System.out.print("\t");
                        }
                        for(int j=r;j>=i;j--){
				if(r%2==0){
                                	System.out.print((char)(num+32)+"\t");
				}
				else{
                                	System.out.print((char)num+"\t");
				}
				num++;
			
                        }
			num=num-r+i;
                        System.out.println();
                }
        }
}

