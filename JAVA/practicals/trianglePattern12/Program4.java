import java.io.*;

class pattern{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("how many rows(pattern) : ");
                int r=Integer.parseInt(br.readLine());


                for(int i=1;i<=r;i++){
			int ch=64+r;
			int x=96+r;
                        for(int j=1;j<=i;j++){
				if(i%2==0){
                                	System.out.print((char)ch +" ");
					ch--;
				}
				else{
					System.out.print((char)x +" ");
					x--;
				}
                        }
                        System.out.println();
                }
        }
}

