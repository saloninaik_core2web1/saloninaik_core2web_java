/*import java.io.*;

class array{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter the array size :");
                int s=Integer.parseInt(br.readLine());

                char arr[]=new char[s];
                System.out.println("ENTER THE CHARACTERS");

                for(int i=0;i<s;i++){
                        arr[i]=(char)br.read();
                }

                System.out.print("ARRAY CHARACTERS ARE :");
                for(int i=0;i<s;i++){
                        System.out.print(arr[i] +" ");
                }
                System.out.println();
        }

}
*/

import java.util.Scanner;

class array {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the array size: ");
        int s = scanner.nextInt();

        char arr[] = new char[s];
        System.out.println("ENTER THE CHARACTERS");

        for (int i = 0; i < s; i++) {
            arr[i] = scanner.next().charAt(0);
        }

        System.out.print("ARRAY CHARACTERS ARE: ");
        for (int i = 0; i < s; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}



