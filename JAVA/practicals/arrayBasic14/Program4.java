import java.io.*;

class array{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter the array size :");
                int s=Integer.parseInt(br.readLine());

                int arr[]=new int[s];
                System.out.println("ENTER THE ELEMENTS");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                int sum=0;
                for(int i=0;i<arr.length;i++){
			if(arr[i]%2!=0){
                        	sum=sum+arr[i];
			}
                }
		System.out.print("SUM OF ODD ELEMENTS IS : " + sum );
                System.out.println();
        }

}


