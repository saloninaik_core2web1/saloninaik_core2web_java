class chardemo{
	public static void main(String[] args){
		char valuefirst="1";                                  // if it was single quoted it would have been run
		char valuesecond="0";                                // double quotes means it is in string format
		System.out.println(valuefirst);
	        System.out.println(valuesecond);
	}
}
