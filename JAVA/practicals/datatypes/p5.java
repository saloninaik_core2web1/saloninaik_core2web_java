class core2web{
	public static void main(String[] args){
         	int ch=65;                                     //int-4 bytr   char=2 byte
		char ch1=ch;                                  //error: invalid conversion from int to char as int size is big.
		System.out.println(ch);
		System.out.println(ch1);
	}
}
