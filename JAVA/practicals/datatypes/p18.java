class core2web{
	public static void main(String[] args){
		int num1=959697;                       //int type data type
		float num2=959697.12345f;              // float data type
		double num3=num2;                     //float is changed to double and ie.ok bcz float=4byyte and double=8 byte
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
	}
}
