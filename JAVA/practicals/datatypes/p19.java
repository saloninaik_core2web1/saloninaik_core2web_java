class floatdemo{
	public static void main(String[] args){
		char ch='A';                              //ch=A
		int num1=ch;
                float num2=num1;                        //ascii value of A=65 int data type convert it into floata s 65.0f        
		double num3=num2;                        // float can be converted it into double 
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
	}
}
