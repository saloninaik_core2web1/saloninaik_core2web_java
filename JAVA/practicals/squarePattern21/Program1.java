import java.util.*;


class squarePattern{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("HOW MANY ROWS: ");
		int r=sc.nextInt();

		for(int i=1;i<=r;i++){
			int ch=64+r;
			for(int j=1;j<=r;j++){
				if(i%2!=0){
					System.out.print((char)ch+"\t");
					ch--;
				}
				else{
					System.out.print(r+"\t");
				}
	
			}
			System.out.println();
		}
	}
}
