import java.io.*;

class revNo{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("enter number :");
                int num=Integer.parseInt(br.readLine());
          
		int rev=0;
                while(num>0){
			int rem=num%10;
			rev=rev*10+rem;
			num=num/10;
		}
		System.out.println("reverse of "+num+"  is "+rev);
	}
}

