import java.util.*;


class spacePattern{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("how many rows: ");
                int row=sc.nextInt();


                for(int i=1;i<=row;i++){

                        int num=row;
			
                        for(int space=1;space<=row-i;space++){
                                System.out.print("\t");
                        }
                        for(int j=1;j<=i;j++){
                                System.out.print(num+"\t");
                                num--;
                        }
                        System.out.println();
                }
        }
}


