import java.util.*;

class demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("HOW MANY ROWS:");
		int r=sc.nextInt();

		for(int i=1;i<=r;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=(r-i)*2+1;j++){
				System.out.print("1"+"\t");
			}
			System.out.println();
		}
	}
}
